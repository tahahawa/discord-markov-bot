-- This file should undo anything in `up.sql`
ALTER TABLE messages DROP COLUMN guild_id Int8;
ALTER TABLE messages DROP COLUMN attachment_urls Text[];
