-- Your SQL goes here
CREATE TABLE guilds(
  id bigint NOT NULL PRIMARY KEY,
  allow_mention boolean DEFAULT true NOT NULL,
  post_limit bigint
);
