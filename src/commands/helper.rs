use crate::ConnectionPool;
use serenity::client::Context;
use serenity::model::prelude::*;

pub async fn download_all_messages(
    guild: &Guild,
    ctx: &Context,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    let channels = guild.channels(&ctx.http).await?;

    // TODO: use MessageIter, and add implement after for it
    for chan in channels {
        let mut _messages = Vec::new();
        let channel_id = (chan.0).0 as i64;

        info!("{:?}", chan.1.name);

        if chan.1.bitrate != None {
            continue;
        }

        let biggest_id = chan.1.last_message_id;

        if biggest_id == None {
            info!("skipped, no latest message exists");
            continue;
        }

        let biggest_id = biggest_id.expect("Biggest ID = None").0 as i64;
        //println!("biggest ID: {}", biggest_id);

        if biggest_id_exists_in_db(biggest_id, ctx).await {
            continue;
        }

        let id = get_latest_id_for_channel(channel_id, ctx).await;

        if id == 0 {
            //println!("no message ID");
            let r#try = chan.0.messages(&ctx.http, |g| g.after(0).limit(100)).await;
            match r#try {
                Err(_) => warn!("error getting messages"),
                _ => _messages = r#try.unwrap(),
            }
        } else {
            let r#try = chan
                .0
                .messages(&ctx.http, |g| g.after(MessageId(id as u64)).limit(100))
                .await;

            match r#try {
                Err(_) => warn!("error getting messages"),
                _ => _messages = r#try.unwrap(),
            }
        }

        while !_messages.is_empty() {
            let _ = chan.0.broadcast_typing(&ctx.http).await;
            info!(
                "storing {} messages from #{} on {}",
                _messages.len(),
                chan.1.name,
                guild.name
            );
            let message_vec = _messages.to_vec();
            for message in message_vec {
                let attachment_urls = if message.attachments.is_empty() {
                    None
                } else {
                    let mut urls: Vec<String> = vec![];
                    for attachment in message.attachments {
                        urls.push(attachment.url);
                    }
                    Some(urls)
                };

                let g_id = if message.guild_id.is_some() {
                    Some(message.guild_id.unwrap().0 as i64)
                } else {
                    None
                };

                let message_id = message.id.0 as i64;
                let channel_id = message.channel_id.0 as i64;
                let author_id = message.author.id.0 as i64;
                let content = &message.content;
                let timestamp = message.timestamp;

                if let Err(why) = sqlx::query!("INSERT INTO messages (id, channel_id, author, content, timestamp, guild_id, attachment_urls) VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT (id) DO UPDATE SET content=$4", message_id, channel_id, author_id, content, timestamp, g_id, &attachment_urls.unwrap_or_default())
                    .execute(&pool)
                    .await {
                        error!("Error inserting message to database: {}", why);
                    };

                // debug!("message: {:?}", message);
            }

            let id2 = get_latest_id_for_channel(channel_id, ctx).await;

            if id2 == 0 {
                //println!("no message ID");
                let r#try = chan.0.messages(&ctx.http, |g| g.after(0).limit(100)).await;
                match r#try {
                    Err(_) => warn!("error getting messages"),
                    _ => _messages = r#try.unwrap(),
                }
            } else if id2 >= biggest_id {
                break;
            } else {
                let r#try = chan
                    .0
                    .messages(&ctx.http, |g| g.after(MessageId(id2 as u64)).limit(100))
                    .await;

                match r#try {
                    Err(_) => warn!("error getting messages"),
                    _ => _messages = r#try.unwrap(),
                }

                //println!("id2: {:?}", id2);
                //println!("{:?}", _messages);
            }
        }
    }
    info!("Downloaded all messages for {:?}", guild.name);
    Ok(())
}

async fn biggest_id_exists_in_db(biggest_id: i64, ctx: &Context) -> bool {
    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    let biggest_id_db_query = sqlx::query!(
        "SELECT id FROM messages WHERE id=$1 ORDER BY id DESC LIMIT 1",
        biggest_id
    )
    .fetch_optional(&pool)
    .await
    .unwrap();

    biggest_id_db_query.is_some()
}

async fn get_latest_id_for_channel(chan_id: i64, ctx: &Context) -> i64 {
    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    let latest_id_query = sqlx::query!(
        "SELECT id FROM messages WHERE channel_id=$1 ORDER BY id DESC LIMIT 1",
        chan_id
    )
    .fetch_optional(&pool)
    .await
    .unwrap();

    match latest_id_query {
        Some(x) => x.id,
        None => 0,
    }
}
