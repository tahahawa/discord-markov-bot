use crate::ConnectionPool;
use crate::IngestLimit;
use futures::TryStreamExt;
use markov::Chain;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::Message,
    prelude::Context,
    utils::{content_safe, ContentSafeOptions}, // To change the embed help color
};

#[command]
#[min_args(0)]
pub async fn hivemind(ctx: &Context, message: &Message, mut args: Args) -> CommandResult {
    let _ = message.channel_id.broadcast_typing(&ctx.http).await;

    debug!("args: {:?}", args);

    let count: usize = args.single_quoted().unwrap_or(1);

    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    let ingest_limit = {
        let data_read = ctx.data.read().await;
        data_read.get::<IngestLimit>().unwrap().to_owned()
    };

    let guild_results = {
        sqlx::query!(
            "SELECT post_limit FROM guilds WHERE id=$1 LIMIT 1",
            message.guild_id.unwrap().0 as i64
        )
        .fetch_optional(&pool)
        .await
        .unwrap()
    };

    let limit = {
        if let Some(x) = guild_results {
            x.post_limit
        } else {
            None
        }
    };

    if let Some(l) = limit {
        if (l as usize) < count {
            message
                .channel_id
                .say(&ctx, format!("You went past the limit of {} messages", l))
                .await?;
            return Ok(());
        }
    }

    let mut chain: Chain<String> = Chain::new();

    let mut results = sqlx::query!("SELECT content, attachment_urls FROM messages WHERE content NOT LIKE '%~hivemind%' AND content NOT LIKE '%~impersonate%' AND content NOT LIKE '%~ping%' AND content NOT LIKE '%~stats%' ORDER BY RANDOM() LIMIT $1 ", ingest_limit)
    .fetch(&pool);

    let mut fed_chain = false;

    while let Some(m) = results.try_next().await? {
        trace!(
            "Feeding message '{}' with attachment_urls '{:?}' into chain",
            m.content,
            m.attachment_urls
        );
        let in_str = if m.attachment_urls.is_some() {
            m.content + "\n" + &m.attachment_urls.unwrap().join("\n")
        } else {
            m.content
        };

        if !in_str.is_empty() {
            chain.feed_str(&in_str);
            fed_chain = true;
        }
    }

    if !fed_chain {
        info!("Requested command has no data available");
        let _ = message.reply(ctx, "They haven't said anything").await;
    }

    let _ = message.channel_id.broadcast_typing(&ctx.http).await;

    for mut line in chain.str_iter_for(count) {
        trace!("Outgoing message: '{}'", line);

        while line.is_empty() {
            line = chain.generate_str();
        }

        let _ = message
            .channel_id
            .say(
                &ctx.http,
                content_safe(&ctx.cache, &line, &ContentSafeOptions::default()).await,
            )
            .await;
    }
    Ok(())
}
